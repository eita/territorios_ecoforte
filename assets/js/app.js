var map, featureList, boroughSearch = [], theaterSearch = [], museumSearch = [];

$(window).resize(function() {
  sizeLayerControl();
});

$(document).on("click", ".feature-row", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).attr("id"), 10));
});

if ( !("ontouchstart" in window) ) {
  $(document).on("mouseover", ".feature-row", function(e) {
    highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
  });
}

$(document).on("mouseout", ".feature-row", clearHighlight);

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

/*$("#full-extent-btn").click(function() {
  map.fitBounds(boroughs.getBounds());
  $(".navbar-collapse.in").collapse("hide");
  return false;
});*/

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  animateSidebar();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  animateSidebar();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  animateSidebar();
  return false;
});

function animateSidebar() {
  $("#sidebar").animate({
    width: "toggle"
  }, 350, function() {
    map.invalidateSize();
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar() {
  /* Empty sidebar features */
  $("#feature-list tbody").empty();
  /* Loop through theaters layer and add only features which are in the map bounds */
  theaters.eachLayer(function (layer) {
    if (map.hasLayer(theaterLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/theater.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through museums layer and add only features which are in the map bounds */
  museums.eachLayer(function (layer) {
    if (map.hasLayer(museumLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/museum.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Update list.js featureList */
  featureList = new List("features", {
    valueNames: ["feature-name"]
  });
  featureList.sort("feature-name", {
    order: "asc"
  });
}

/* Basemap Layers */
//var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var osmUrl='https://api.mapbox.com/styles/v1/dtygel/cjgll13zc000c2sqnxobgx4ie/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZHR5Z2VsIiwiYSI6IjY4YjRhNjU1NWJhYTU2OGU1YWQ1OTcxYjRjNDcyNzIxIn0.Go-whnL9yIt3rHicQ5gqbA';
var osmAttrib='Dados do mapa © Contribuidoras/es do <a href="http://openstreetmap.org">OpenStreetMap</a>';
var osm = new L.TileLayer(osmUrl, {minZoom: 2, maxZoom: 19, attribution: osmAttrib});



var usgsImagery = L.layerGroup([L.tileLayer("http://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}", {
  maxZoom: 15,
}), L.tileLayer.wms("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EROS_Ortho_SCALE/ImageServer/WMSServer?", {
  minZoom: 16,
  maxZoom: 19,
  layers: "0",
  format: 'image/jpeg',
  transparent: true,
  attribution: "Aerial Imagery courtesy USGS"
})]);

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.7,
  radius: 10
};

var boroughs = [];
var territorios = [
    {
      "name": "Rede de Desenvolvimento Sustentável e Solidário das Encostas da Serra Geral de Santa Catarina – Rede AGRECO	Anitápolis/SC",
      "cities": ["Grão Para/SC", "Gravatal/SC", "Rancho Queimado/SC", "Rio Fortuna/SC", "Santa Rosa de Lima/SC", "São Bonifácio/SC"]
    },
    {
      "name": "Rede de Intercambio de Sementes – RIS",
      "cities": ["Sobral/CE", "Itapipoca/CE", "Trairi/CE", "Tururu/CE", "Irauçuba/CE", "Massapê/CE", "Frecheirinha/CE", "Santana do Acarau/CE", "Morrinhos/CE", "Bela Cruz/CE", "Marco/CE", "Forquilha/CE"]
    },
    {
      "name": "Rede de Produção Agroecológica do Semiárido Piauiense - REPASPI",
      "cities": ["São Raimundo Nonato/PI", "São Lourenço do Piauí/PI"]
    },
    {
      "name": "Rede Ecovida de Agroecologia do Rio Grande do Sul",
      "cities": ["Aratiba/RS", "Barão de Cotegipe/RS", "Erexim/RS", "Itatiba do Sul/RS ", "Mariano Moro/RS", "Paulo Bento/RS ", "Severiano de Almeida/RS ", "Três Arroios/RS", "Viadutos/RS", "Dom Pedro de Alcântara/RS", "Itati/RS", "Mampituba/RS", "Maquiné/RS", "Morrinhos do Sul/RS", "Osório/RS ", "Santo Antônio da Patrulha/RS ", "Terra de Arreia/RS ", "Torres/RS ", "Três Cachoeiras/RS ", "Três Forquilhas/RS", "Antônio Prado/RS", "Bento Gonçalves/RS", "Canela/RS", "Carlos Barbosa/RS", "Caxias do Sul/RS ", "Farroupilha/RS", "Flores da Cunha/RS", "Garibaldi/RS ", "Gramado/RS", "Ipê/RS ", "Monte Alegre dos Campos/RS", "Nova Bassano/RS", "Nova Pádua/RS ", "Nova Petrópolis/RS", "Nova Prata/RS", "Nova Roma do Sul/RS", "Picada Café/RS", "Pinhal da Serra/RS", "Roca Sales/RS", "Santa Tereza/RS ", "São Francisco de Paula/RS", "Vacaria/RS", "Veranópolis/RS", "Casca/RS", "Passo Fundo/RS", "Sananduva/RS", "Santo Antônio do Palma/RS", "Santo Expedito do Sul/RS", "São Domingos do Sul/RS", "São João da Urtiga/RS", "Vila Maria/RS", "Alecrim/RS", "Campina das Missões/RS", "Cândido Godói/RS", "Crissiumal/RS", "Dezesseis de Novembro/RS", "Giruá/RS", "Horizontina/RS", "Porto Vera Cruz/RS", "Porto Xavier/RS", "Santa Rosa/RS ", "São Paulo das Missões/RS ", "Seberi/RS ", "Tenente Portela/RS ", "Três de Maio/RS ", "Três Passos/RS", "Tucunduva/RS", "Arroio do Meio/RS", "Cachoeira do Sul/RS", "Candelária/RS", "Dona Francisca/RS", "Rio Pardo/RS", "Santa Cruz do Sul/RS", "Sinimbu/RS ", "Vale do Sol/RS", "Venâncio Aires/RS", "Vera Cruz/RS", "Arroio do Padre/RS", "Canguçu/RS", "Cerrito/RS", "Cerro Grande do Sul/RS", "Herval/RS", "Morro Redondo/RS", "Pelotas/RS", "Piratini/RS", "São Lourenço do Sul/RS", "Sentinela do Sul/RS", "Turuçu/RS"]
    },
    {
      "name": "Rede UAI Vale do Urucuia",
      "cities": ["Arinos/MG", "Bonfinóplis de Minas/MG", "Buritis/MG ", "Chapada Gaucha/MG", "Natalândia/MG ", "Riachinho/MG ", "Uruana de Minas/MG ", "Urucuia/MG"]
    },
    {
      "name": "Rede Agroecológica do Leste de Minas Gerais",
      "cities": ["Simonésia/MG", "Manhuaçu/MG", "São João do Manhuaçu/MG", "Santana do Manhuaçu/MG", "Conceição de Ipanema/MG", "São José do Mantimento/MG ", "Caratinga/MG"]
    },
    {
      "name": "Rede Maniva de Agroecologia – REMA",
      "cities": ["Manaus/AM", "Presidente/AM", "Figueiredo/AM", "Rio Preto da Eva/AM", "Itacoatiara/AM"]
    },
    {
      "name": "Rede de Agroecologia do Leste Paulista – Alta Mogiana",
      "cities": ["Campinas/SP ", "Restinga/SP ", "Serrana/SP ", "Caconde/SP ", "Mogi Mirim/SP ", "Artur Nogueira/SP ", "Cosmópolis/SP ", "Amparo/SP ", "Americana/SP ", "Jaguariúna/SP ", "Piracai/SP"]
    },
    {
      "name": "Rede de Agroecologia do Território da Cantuquiriguaçu",
      "cities": ["Laranjeiras do Sul/PR", "Rio Bonito do Iguaçu/PR", "Saudade do Iguaçu/PR", "Goioxim/PR", "Nova Laranjeiras/PR", "Porto Barreiro/PR", "Quedas do Iguaçu/PR", "Palmital/PR", "Laranjal/PR"]
    },
    {
      "name": "GIAS - Grupo de Intercâmbio de Agroecologia",
      "cities": ["Cuiabá/MT", "Cáceres/MT", "Pontes e Lacerda/MT", "Mirassol D’Oeste/MT", "Araputanga/MT", "Porto Esperidião/MT", "Poconé/MT", "Nossa Senhora do Livramento/MT", "Jangada/MT", "Chapada dos Guimarães/MT", "Várzea Grande/MT", "Lucas do Rio Verde/MT", "Claudia/MT", "Sinop/MT", "Rondonópolis/MT", "Tangará da Serra/MT", "Pedra Preta/MT", "Cotriguaçu/MT", "Poxoréu/MT", "Barão do Melgaço/MT", "Terra Nova do Norte/MT", "Alta Floresta/MT", "Comodoro/MT", "Porto Alegre do Norte/MT", "Juína/MT"]
    },
    {
      "name": "Rede Sabor Natural do Sertão",
      "cities": ["Campo Alegre de Lourdes/BA", "Canudos/BA ", "Casa Nova/BA", "Curaça/BA", "Juazeiro/BA ", "Pílão Arcado/BA ", "Remanso/BA ", "Santo Sé/BA ", "Sobradinho/BA ", "Uauá/BA"]
    },
    {
      "name": "Rede Criativa e Solidária Poloprobio/Encauchados – RedEncauchados",
      "cities": ["Acará/PA", "Anajás/PA", "Ananindeua/PA", "Belém/PA", "Belterra/PA", "Breves/PA", "Castanhal/PA", "Curralinho/PA", "Inhangapi/PA", "Oriximiná/PA", "Santarém/PA", "Santo Antonio do Tauá/PA", "São Francisco do Pará/PA", "São Miguel do Guamá/PA"]
    },
    {
      "name": "Rede de Agricultores Experimentadores do Araripe",
      "cities": ["Araripina/PE", "Granito/PE", "Ipubi/PE", "Ouricuri/PE", "Trindade/PE", "Bodocó/PE", "Exu/PE", "Moreilândia/PE", "Santa Cruz/PE", "Santa Filomena/PE", "Parnamirim/PE", "Crato/CE", "Juazeiro do Norte/CE", "Barbalha/CE", "Simões/PI", "Marcolândia/PI", "Jaicós/PI", "Padre Marcos/PI", "Fronteiras/PI", "Caldeirão Grande/PI"]
    },
    {
      "name": "Rede Agroecológica-Extrativista Trijunção do Cerrado Central",
      "cities": ["Mambaí/GO ", "Sitio da D'Abadia/GO ", "Damianópolis/GO", "Formoso e Chapada Gaúcha/MG", "Jaborandi/BA"]
    },
    {
      "name": "Rede Bico Agroecológico",
      "cities": ["Esperantina/TO", "São Sebastião/TO", "Buriti do Tocantins/TO", "Araguatins/TO", "Praia Norte/TO", "Axixá do Tocantins/TO", "São Miguel/TO"]
    },
    {
      "name": "Rede de agroecologia da Associação dos Produtores Orgânicos De Mato Grosso Do Sul (APOMS)",
      "cities": ["Bodoquena/MS", "Caarapó/MS", "Corumbá/MS", "Dourados/MS", "Eldorado/MS", "Glória de Dourados/MS", "Itaquirai/MS", "Ivinhema/MS", "Japorã/MS", "Mundo Novo/MS", "Nioaque/MS", "Novo Horizonte do Sul/MS", "Ponta Porã/MS", "Rio Brilhante/MS", "Sidrolândia/MS", "Terenos/MS"]
    },
    {
      "name": "Rede de Agroecologia da Zona da Mata de Minas Gerais",
      "cities": ["Acaiaca/MG", "Araponga/MG", "Caparaó/MG", "Diogo Vasconcelos/MG", "Divino/MG", "Ervália/MG", "Espera Feliz/MG", "Guidoval/MG", "Lima Duarte/MG", "Muriaé/MG", "Orizânia/MG", "Paula Cândido/MG", "Rio Pomba/MG", "Rosário de Limeira/MG", "Santa Margarida/MG", "Santana do Manhuaçu/MG", "Simonésia/MG", "Tombos/MG", "Viçosa/MG", "Visconde do Rio Branco/MG"]
    },
    {
      "name": "Rede Espaço Agroecológico",
      "cities": ["Abreu e Lima/PE", "Bom Jardim/PE", "Chã Grande/PE", "Gravatá/PE", "Igarassu/PE", "Lagoa de Itaenga/PE", "Recife/PE", "Vitória de Santo Antão/PE"]
    },
    {
      "name": "Rede de Comercialização Solidária de Agricultores Familiares e Extrativistas do Cerrado",
      "cities": ["Goiás/GO", "Heitoraí/GO", "São Domingos/GO", "Guarani/GO", "Flores de Goiás/GO", "Itaberaí/GO", "Cromínia/GO", "Silvânia/GO", "Fazenda Nova/GO", "Planaltina/GO", "Araguapaz/GO", "Aruanã/GO", "Montividiu do Norte/GO", "Trombas/GO", "Porangatu/GO", "Mutunopólis/GO", "Uruaçu/GO", "Divinopolis/GO", "Vila Boa/GO", "Formosa/GO", "Goiânia/GO", "Lassance/MG", "Santa Fé de Minas/MG", "Ibiaí/MG", "Buenopólis/MG", "Augusto de Lima/MG", "São Romão/MG", "Buritizeiro/MG", "Chapada Gaúcha/MG", "São Francisco/MG", "Brasilândia/MG", "Corinto/MG", "Várzea da Palma/MG", "Formoso/MG", "Urucuia/MG"]
    },
    {
      "name": "Rede Sociotécnica de Agroecologia do Sertão norte Mineiro",
      "cities": ["Montes Claros/MG", "Januária/MG", "Ibiracatu/MG", "Rio Pardo de Minas/MG", "Grao Mogol/MG", "Riacho dos Machados/MG", "Mato Verde/MG", "Pai Pedro/MG", "São João das Missões/MG"]
    },
    {
      "name": "Rede de Agricultura Biodinâmica",
      "cities": ["Botucatu/SP", "Córrego do Bom Jesus/SP", "Guapé/MG", "Maria da Fé/MG"]
    },
    {
      "name": "Rede de Cultivos Agroecológicos",
      "cities": ["Cajazeiras/PB", "Santa Helena/PB", "Marizópolis/PB", "Sousa/PB", "São José da Lagoa Tapada/PB", "Aparecida/PB", "Pombal/PB", "Catolé do Rocha/PB"]
    },
    {
      "name": "Rede Terra",
      "cities": ["Cristalina/GO", "Cidade Ocidental/GO", "Luziânia/GO"]
    },
    {
      "name": "Morro da Cutia",
      "cities": ["Terra Nova do Norte/MT", "Alta Floresta/MT", "Carlinda/MT", "Nova Santa Helena/MT", "Marcelândia/MT", "Nova Guarita/MT", "Matupá/MT", "Novo Mundo/MT", "Guarantã do Norte/MT", "Colíder/MT", "Nova Canaã do Norte/MT", "Itaúba/MT", "Nova Olímpia/MT", "Rosário do Oeste/MT", "Jangada/MT", "Acorizal/MT", "Nossa Senhora do Livramento/MT", "Chapada dos Guimarães/MT", "Campo Verde/MT", "Nova Brasilândia/MT"]
    },
    {
      "name": "Rede Agroecologia na Borborema",
      "cities": ["Queimadas/PB", "Massaranduba/PB", "Lagoa Seca/PB", "Matinhas/PB", "Alagoa Nova/PB", "São Sebastião de Lago de Roça/PB", "Montadas/PB", "Areial/PB", "Esperança/PB", "Remígio/PB", "Algodão de Jandaíra/PB", "Arara/PB", "Casserengue/PB", "Solânea/PB"]
    },
    {
      "name": "Rede de desenvolvimento sustentável e solidário",
      "cities": ["Santa Rosa de Lima/SC", "Anitápolis/SC", "Rancho Queimado/SC", "São Bonifácio/SC", "Rio Fortuna/SC", "Gravatal/SC", "Grão Pará/SC"]
    },
    {
      "name": "(Sem nome) CEAF",
      "cities": ["Barão de Antonina/SP", "Bom Sucesso de Itararé/SP", "Buri/SP", "Capão Bonito/SP", "Coronel Macedo/SP", "Guapiara/SP", "Itaberá/SP", "Itapeva/SP", "Itaporanga/SP", "Itararé/SP", "Nova Campina/SP", "Ribeirão Branco/SP", "Ribeirão Grande/SP", "Riversul/SP", "Taquarituba/SP", "Taquarivaí/SP"]
    }
];

var territoryColors = ["#ef9a9a", "#ef5350", "#e53935", "#c62828", "#ff8a80", "#f48fb1", "#ec407a", "#d81b60", "#ad1457", "#ff80ab", "#ce93d8", "#ab47bc", "#8e24aa", "#6a1b9a", "#ea80fc", "#b39ddb", "#7e57c2", "#5e35b1", "#4527a0", "#b388ff", "#9fa8da", "#5c6bc0", "#3949ab", "#283593", "#8c9eff", "#90caf9", "#42a5f5", "#1e88e5", "#1565c0", "#82b1ff", "#81d4fa", "#29b6f6", "#039be5", "#0277bd", "#80d8ff", "#80deea", "#26c6da", "#00acc1", "#00838f", "#84ffff", "#80cbc4", "#26a69a", "#00897b", "#00695c", "#a7ffeb", "#a5d6a7", "#66bb6a", "#43a047", "#2e7d32", "#b9f6ca", "#c5e1a5", "#9ccc65", "#7cb342", "#558b2f", "#ccff90", "#e6ee9c", "#d4e157", "#c0ca33", "#9e9d24", "#f4ff81", "#fff59d", "#ffee58", "#fdd835", "#f9a825", "#ffff8d", "#ffe082", "#ffca28", "#ffb300", "#ff8f00", "#ffe57f", "#ffcc80", "#ffa726", "#fb8c00", "#ef6c00", "#ffd180", "#ffab91", "#ff7043", "#f4511e", "#d84315", "#ff9e80", "#bcaaa4", "#8d6e63", "#6d4c41", "#4e342e", "#eeeeee", "#bdbdbd", "#757575", "#424242", "#b0bec5", "#78909c", "#546e7a", "#37474f"];
for (var i=0; i<territorios.length; i++) {
  (function(i) {
    var name = territorios[i].name;
    var UFs = [];
    for (var j=0;j<territorios[i].cities.length;j++) {
      var l = territorios[i].cities[j].split('/');
      var UF = l[1].toUpperCase().trim();
      if (UFs.indexOf(UF)==-1) {
        UFs.push(UF);
      }
    }
    boroughs[i] = L.geoJson(null, {
      style: function (feature) {
        return {
          fillColor: territoryColors[i],
          fillOpacity: 0.7,
          fill: true,
          stroke: true,
          color: "#333333",
          weight: 1,
          opacity: 1,
          clickable: true
        };
      },
      onEachFeature: function (feature, layer) {
        boroughSearch.push({
          name: name,
          source: "Boroughs",
          id: L.stamp(layer),
          bounds: layer.getBounds()
        });
        layer.on('click', function(e) {
          this.openPopup();
        });
      }
    });
    var estadosText = (UFs.length==1) ? "1 estado" : UFs.length+" estados";
    boroughs[i].bindPopup("<b>"+name+"</b><br/>"+territorios[i].cities.length+" municípios em "+estadosText+" ("+UFs.join(', ')+")");
  })(i);

  for (var j=0;j<territorios[i].cities.length;j++) {
    var l = territorios[i].cities[j].split('/');
    var city = l[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "").split(" ").join("_").toUpperCase().trim();
    var UF = l[1].toUpperCase().trim();
    (function (i) {
      $.getJSON("data/municipios/"+UF+"/geojson/"+city+".json", function (data) {
        boroughs[i].addData(data);
      });
    })(i);
  }
}

//Create a color dictionary based off of subway route_id
var subwayColors = {"1":"#ff3135", "2":"#ff3135", "3":"ff3135", "4":"#009b2e",
    "5":"#009b2e", "6":"#009b2e", "7":"#ce06cb", "A":"#fd9a00", "C":"#fd9a00",
    "E":"#fd9a00", "SI":"#fd9a00","H":"#fd9a00", "Air":"#ffff00", "B":"#ffff00",
    "D":"#ffff00", "F":"#ffff00", "M":"#ffff00", "G":"#9ace00", "FS":"#6e6e6e",
    "GS":"#6e6e6e", "J":"#976900", "Z":"#976900", "L":"#969696", "N":"#ffff00",
    "Q":"#ffff00", "R":"#ffff00" };

var subwayLines = L.geoJson(null, {
  style: function (feature) {
      return {
        color: subwayColors[feature.properties.route_id],
        weight: 3,
        opacity: 1
      };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Division</th><td>" + feature.properties.Division + "</td></tr>" + "<tr><th>Line</th><td>" + feature.properties.Line + "</td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.Line);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");

        }
      });
    }
    layer.on({
      mouseover: function (e) {
        var layer = e.target;
        layer.setStyle({
          weight: 3,
          color: "#00FFFF",
          opacity: 1
        });
        if (!L.Browser.ie && !L.Browser.opera) {
          layer.bringToFront();
        }
      },
      mouseout: function (e) {
        subwayLines.resetStyle(e.target);
      }
    });
  }
});
/*$.getJSON("data/subways.geojson", function (data) {
  subwayLines.addData(data);
});*/

/* Single marker cluster layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true,
  disableClusteringAtZoom: 16
});

/* Empty layer placeholder to add to layer control for listening when to add/remove theaters to markerClusters layer */
var theaterLayer = L.geoJson(null);
var theaters = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/theater.png",
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.NAME,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Name</th><td>" + feature.properties.NAME + "</td></tr>" + "<tr><th>Phone</th><td>" + feature.properties.TEL + "</td></tr>" + "<tr><th>Address</th><td>" + feature.properties.ADDRESS1 + "</td></tr>" + "<tr><th>Website</th><td><a class='url-break' href='" + feature.properties.URL + "' target='_blank'>" + feature.properties.URL + "</a></td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.name);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/theater.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      theaterSearch.push({
        name: layer.feature.properties.name,
        address: layer.feature.properties.ADDRESS1,
        source: "Theaters",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
/*$.getJSON("data/DOITT_THEATER_01_13SEPT2010.geojson", function (data) {
  theaters.addData(data);
  map.addLayer(theaterLayer);
});*/

/* Empty layer placeholder to add to layer control for listening when to add/remove museums to markerClusters layer */
var museumLayer = L.geoJson(null);
var museums = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/museum.png",
        iconSize: [24, 28],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.NAME,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Name</th><td>" + feature.properties.NAME + "</td></tr>" + "<tr><th>Phone</th><td>" + feature.properties.TEL + "</td></tr>" + "<tr><th>Address</th><td>" + feature.properties.ADRESS1 + "</td></tr>" + "<tr><th>Website</th><td><a class='url-break' href='" + feature.properties.URL + "' target='_blank'>" + feature.properties.URL + "</a></td></tr>" + "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.NAME);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/museum.png"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      museumSearch.push({
        name: layer.feature.properties.NAME,
        address: layer.feature.properties.ADRESS1,
        source: "Museums",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
/*$.getJSON("data/DOITT_MUSEUM_01_13SEPT2010.geojson", function (data) {
  museums.addData(data);
});*/

visibleLayers = boroughs;
visibleLayers.push(osm);
map = L.map("map", {
  zoom: 2,
  center: [-15.780148, -47.92917],
  layers: visibleLayers,
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
  if (e.layer === theaterLayer) {
    markerClusters.addLayer(theaters);
    syncSidebar();
  }
  if (e.layer === museumLayer) {
    markerClusters.addLayer(museums);
    syncSidebar();
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === theaterLayer) {
    markerClusters.removeLayer(theaters);
    syncSidebar();
  }
  if (e.layer === museumLayer) {
    markerClusters.removeLayer(museums);
    syncSidebar();
  }
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

/* Attribution control */
function updateAttribution(e) {
  $.each(map._layers, function(index, layer) {
    if (layer.getAttribution) {
      $("#attribution").html((layer.getAttribution()));
    }
  });
}
map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

var attributionControl = L.control({
  position: "bottomright"
});
attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "<span class='hidden-xs'>Desenvolvido pela <a href='http://eita.org.br'>Cooperativa EITA</a> baseado em <a href='https://github.com/bmcbride/bootleaf'>Bootleaf</a> | </span><a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Atribuição</a>";
  return div;
};
map.addControl(attributionControl);

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
/*var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-location-arrow",
  metric: false,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);*/

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Street Map": osm,
  "Aerial Imagery": usgsImagery
};

var groupedOverlays = {
  "Points of Interest": {
    "<img src='assets/img/theater.png' width='24' height='28'>&nbsp;Theaters": theaterLayer,
    "<img src='assets/img/museum.png' width='24' height='28'>&nbsp;Museums": museumLayer
  },
  "Reference": {
    "d": boroughs[0]
  }
};

/*var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);*/
/*var layerControl = L.control.groupedLayers(baseLayers, {
  collapsed: isCollapsed
}).addTo(map);*/

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();
  /* Fit map to boroughs bounds */
  var bounds = boroughs[0].getBounds();
  for(var i=1;i<boroughs.length-1;i++) {
    bounds.extend(boroughs[i].getBounds());
  }
  map.fitBounds(bounds);
  //map.fitBounds(boroughs.getBounds());
  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var boroughsBH = new Bloodhound({
    name: "Boroughs",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: boroughSearch,
    limit: 10
  });

  var theatersBH = new Bloodhound({
    name: "Theaters",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: theaterSearch,
    limit: 10
  });

  var museumsBH = new Bloodhound({
    name: "Museums",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: museumSearch,
    limit: 10
  });

  var geonamesBH = new Bloodhound({
    name: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "http://api.geonames.org/searchJSON?username=bootleaf&featureClass=P&maxRows=5&countryCode=US&name_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geonames, function (result) {
          return {
            name: result.name + ", " + result.adminCode1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });
  boroughsBH.initialize();
  theatersBH.initialize();
  museumsBH.initialize();
  geonamesBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Boroughs",
    displayKey: "name",
    source: boroughsBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>Boroughs</h4>"
    }
  }, {
    name: "Theaters",
    displayKey: "name",
    source: theatersBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/theater.png' width='24' height='28'>&nbsp;Theaters</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "Museums",
    displayKey: "name",
    source: museumsBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/museum.png' width='24' height='28'>&nbsp;Museums</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }, {
    name: "GeoNames",
    displayKey: "name",
    source: geonamesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Boroughs") {
      map.fitBounds(datum.bounds);
    }
    if (datum.source === "Theaters") {
      if (!map.hasLayer(theaterLayer)) {
        map.addLayer(theaterLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Museums") {
      if (!map.hasLayer(museumLayer)) {
        map.addLayer(museumLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}
